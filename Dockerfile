FROM ubuntu:15.04

# Let the conatiner know that there is no tty
ENV DEBIAN_FRONTEND=noninteractive

# Avoid ERROR: invoke-rc.d: policy-rc.d denied execution of start.
RUN echo "#!/bin/sh\nexit 0" > /usr/sbin/policy-rc.d

# base packages
RUN apt-get update && apt-get -y install \
    apt-utils \
    build-essential \
    ntp \
    nscd \

    # includes apt-add-repository
    software-properties-common

# add locales
RUN locale-gen en_US && locale-gen en_US.UTF-8 && \
    locale-gen en_GB && locale-gen en_GB.UTF-8 && \
    locale-gen en_ZA && locale-gen en_ZA.UTF-8 && \
    locale-gen en_AU && locale-gen en_AU.UTF-8 && \
    update-locale